require 'rails_helper'

describe SignupOperation do
  context 'validations' do

    let(:signup_params) do
      { name: 'Rafael', email: 'rafael@acme.ltda',
        password: '123123', password_confirmation: '123123' }
    end

    it 'validates correctly when passing all parameters' do
      signup = SignupOperation.new(User.new)
      user_created = signup.call(signup_params)
      expect(user_created).to be_valid
    end

    it 'is not valid when missing fields' do
      signup = SignupOperation.new(User.new)

      object = signup.call({})
      expect(object.errors).to be_present
    end

    it 'attemps to save to the database' do
      #instead of a spy we could pass a class with saves to a hash
      service = spy('service')
      expect(service).to receive(:call)
      signup = SignupOperation.new(User.new, service)
      signup.call(signup_params)
    end
  end
end
