require 'rails_helper'

describe 'User::Signup' do
  context 'validating' do
    it 'doesnt allow to create an user without email' do
      signup = User::Signup.new
      expect do
        signup.call(name: 'rafael', password: '123123')
      end.to raise_error(ActiveRecord::RecordInvalid)
    end

    it 'doesnt allow to create an user without a name' do
      signup = User::Signup.new
      expect do
        signup.call(name: '',  email: 'lala@teste.com', password: '123123')
      end.to raise_error(ActiveRecord::RecordInvalid)
    end
  end

  context 'user registering successfully' do
    it 'persists an user to the database' do
      signup = User::Signup.new
      signup.call(email: 'rafael@acme.ltda', name: 'Rafael',
        password: '123123', password_confirmation: '123123')

      user_registered = User.find_by(email: 'rafael@acme.ltda')

      expect(user_registered).to be_present
      expect(user_registered.name).to eq('Rafael')
    end
  end
end
