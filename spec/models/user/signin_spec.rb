require 'rails_helper'

describe User::Signin do
  context 'authenticating' do
    #Eu particularmente gosto de usar meus proprios objetos
    #no lugar de factories ou fixtures.
    let(:signup_params) do
      { email: 'rafael@acme.ltda',
        password: '123123',
        name: 'Rafael' }
    end

    it 'authenticantes a valid user' do
      User::Signup.new.call(signup_params)

      signin = User::Signin.new
      authenticated_user = signin.call(signup_params[:email], signup_params[:password])

      expect(authenticated_user.name).to eq('Rafael')
    end

    it 'doesnt authenticate with an invalid password' do
      User::Signup.new.call(signup_params)

      signin = User::Signin.new
      signed_in = signin.call(signup_params[:email], 'wrong password')

      expect(signed_in).to_not be_present
    end

    it 'doesnt authenticate with an invalid email' do
      User::Signup.new.call(signup_params)

      signin = User::Signin.new
      signed_in = signin.call('idontexists@acme.ltda', 'nono')

      expect(signed_in).to_not be_present
    end
  end
end
