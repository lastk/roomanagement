require 'rails_helper'

describe SignupController do
  context 'registering a valid user' do
    it 'creates an user' do
      post :create, { user: {name: 'Rafael', email: 'rafael@acme.ltda',
                             password: '123123', password_confirmation: '123123'} }

      expect(response).to have_http_status(:found)
      expect(User.last.name).to eq('Rafael')
    end
  end
end
