class CreateEnrollments < ActiveRecord::Migration[5.0]
  def change
    create_table :enrollments do |t|
      t.date :day
      t.integer :enrollment_time
      t.references :user, foreign_key: true

      t.timestamps
    end

    add_index :schedulings, [:enrollment_time, :day], unique: true

  end
end
