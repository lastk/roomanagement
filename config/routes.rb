Rails.application.routes.draw do
  resources :signup, only: [:new, :create]
  resources :enrollments
  resources :sessions
end

