class WardenStrategy < Warden::Strategies::Base
  def valid?
    user_params['email'] && user_params['password']
  end

  def authenticate!
    user = User::Signin.new.call(user_params['email'],
                                 user_params['password'])
    if user
      success! user
    else
      fail 'Try again'
    end
  end

  private

  def user_params
    params['user']
  end
end
