module ApplicationHelper

  def this_week_days
    ref = Date.today
    ref.beginning_of_week..ref.last_weekday
  end
end
