class SessionsController < ApplicationController
  def create
    warden.authenticate!
    redirect_to enrollments_url, notice: "Logged in!"
  end

  def destroy
    warden.logout
    redirect_to root_url, notice: "Logged out!"
  end
end
