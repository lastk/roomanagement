class SignupController < ApplicationController
  respond_to :html

  def new
    @new_user = SignupOperation.new(User.new)
  end

  def create
    @new_user = SignupOperation.new(User.new).call(user_params)
    respond_with @new_user, location: -> { enrollments_path }
  end

  private

  def user_params
    params.require(:user).permit(:email, :name, :password, :password_confirmation)
  end
end
