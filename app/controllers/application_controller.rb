class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def current_user
    warden.user
  end
  helper_method :current_user

  def warden
    env['warden']
  end

  def authenticate_user!
    unless current_user
      redirect_to new_session_url
    end
  end

end
