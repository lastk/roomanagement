class EnrollmentsController < ApplicationController
  before_action :authenticate_user!

  respond_to :json

  def index
    enrollments = Enrollment.enrollments_of_week
    @enrollments_presenter = EnrollmentsPresenter.new(enrollments, current_user)
  end

  def create
    respond_with Enrollment.new id: 10 #delete to Enrollment::Creation
  end
end
