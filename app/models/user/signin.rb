class User::Signin 
  def call(email, password)
    user = User.find_by(email: email)
    user && user.authenticate(password)
  end
end
