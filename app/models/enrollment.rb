class Enrollment < ApplicationRecord
  belongs_to :user

  def self.enrollments_of_week(ref = Date.today)
    ref = Date.today
    where(day: ref.beginning_of_week..ref.last_weekday)
  end
end
