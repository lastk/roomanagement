class EmptyEnrollmentPresenter < BasePresenter
  #simple delegator forces an object in constructor
  def initialize
  end

  def link(date, hour)
    helpers.link_to(url_helpers.enrollments_path, class: 'available', 'data-hour' => hour, 'data-date' => date) do
      helpers.content_tag(:span, class: 'label label-success') do
        'Available'
      end
    end
  end
end
