class EnrollmentPresenter < BasePresenter
  def initialize(enrollment, user)
    @enrollment = enrollment
    @user = user
    __setobj__(enrollment)
  end

  def link
    if @user == @enrollment.user
      present_enrolled_same_user
    else
      present_enrolled_different_user
    end
  end

  def user_name
    @enrollment.user.name
  end

  private

  def present_enrolled_same_user
    helpers.link_to(url_helpers.enrollment_path(@enrollment), method: :delete, class: 'unroll', 'data-enroll-id' => @enrollment.id) do
      helpers.content_tag(:span, class: 'label label-danger') do
        'Unroll'
      end
    end
  end

  def present_enrolled_different_user
    helpers.content_tag(:span, class: 'label label-danger') do
      "Enrolled for: #{user_name}"
    end
  end
end
