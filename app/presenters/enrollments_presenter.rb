class IndexedEnrollments
  def initialize(enrollments)
    @indexed = {}
    enrollments.each do |e|
      date = e.day.to_s
      @indexed[date] ||= {}
      @indexed[date][e.enrollment_time] = e
    end
  end

  def get(date, time)
    return nil unless @indexed[date.to_s].present?
    @indexed[date.to_s][time]
  end
end

class EnrollmentsPresenter < BasePresenter
  attr_reader :enrollments
  def initialize(enrollments, user)
    @indexed_enrollments = IndexedEnrollments.new(enrollments)
    @user = user
    __setobj__(enrollments)
  end

  def render_link_for(date, hour)
    enrollment = @indexed_enrollments.get(date, hour)
    if enrollment.present?
      EnrollmentPresenter.new(enrollment, @user).link
    else
      EmptyEnrollmentPresenter.new.link(date, hour)
    end
  end
end
