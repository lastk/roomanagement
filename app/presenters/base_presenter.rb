class BasePresenter < SimpleDelegator
  def helpers
    ApplicationController.helpers
  end

  def url_helpers
    Rails.application.routes.url_helpers
  end
end
