#Making operations to be a Reform object, we have a nice validation here
#and make it totally compatible with rails forms.
#cons: hard to this object to be thread safe :(

class SignupOperation < Reform::Form

  model :user

  property :name
  property :email
  property :password
  property :password_confirmation

  validates :name, :email, :password, :password_confirmation, presence: true

  def initialize(model, signup_service = User::Signup.new)
    super(model)
    @signup_service = signup_service
  end

  def call(params)
    if validate(params)
      save do |attrs|
        return @signup_service.call(attrs)
      end
    end
    self
  end
end
