var app = app || {}
app.Enrollments = {
    init: function(enrollClass){
        var $elements = $(enrollClass);
        $elements.click(function(event){
            event.preventDefault();
            app.Enrollments.enroll($(this));
        });
    },
    enroll: function($el) {
        var date = $el.data('date');
        var time = $el.data('hour');
        var url = $el.attr('href');
        $.post(url, {"date":date, "time": time })
            .done(function(data){
                $el.html(app.Enrollments.enrolledTemplate(data));
            })
        debugger;
    },

    enrolledTemplate: function(enrollment){
        var template = "<a class='unroll' rel='nofollow' data-method='delete' href=''><span class='label label-danger'>Unroll</span></a>"
        html = $(template).data('enroll-id', enrollment.id)
            .attr('href', "/enrollments/"+enrollment.id)[0];

        return html;
    }
};
$(document).ready(function(){
    app.Enrollments.init('.available')
});
